package service

import (
	"gilab.com/pragmaticreviews/golang-gin-poc/entitiy"
)

// an interface to save the video and find all the list of the video
type VideoService interface {
	Save(entitiy.Video) entitiy.Video
	FindAll() []entitiy.Video
}

// struct that implement the interface
type videoservice struct {
	videos []entitiy.Video
}

// function for the implementation of an interface along the struct
func New() VideoService {
	return &videoservice{}
}

// to make functional of an interface functions, assign them as a method
func (service *videoservice) Save(video entitiy.Video) entitiy.Video {
	service.videos = append(service.videos, video)
	return video
}

func (service *videoservice) FindAll() []entitiy.Video {
	return service.videos
}

