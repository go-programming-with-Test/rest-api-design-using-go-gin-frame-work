/* the program wants to do:
- list all the videos in the intity
- save the new given video
*/
package main

import (
	"io"
	"os"

	"gilab.com/pragmaticreviews/golang-gin-poc/controller"
	"gilab.com/pragmaticreviews/golang-gin-poc/middleware"
	"gilab.com/pragmaticreviews/golang-gin-poc/service"
	"github.com/gin-gonic/gin"
	gindump "github.com/tpkeeper/gin-dump" // helps to eliminate bugs
)

var (
	VideoService    service.VideoService       = service.New()
	VideoController controller.VideoController = controller.New(VideoService)
)

func setupLogOutPut() {
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func main() {

	setupLogOutPut()
	server := gin.New()

	server.Use(gin.Recovery(), middleware.Logger(), middleware.BasicAuth(), gindump.Dump())

	server.GET("/videos", func(ctx *gin.Context) {
		ctx.JSON(200, VideoController.FindAll())
	})

	server.POST("/albums", func(ctx *gin.Context) {
		ctx.JSON(200, VideoController.Save(*ctx))
	})

	server.Run("localhost:8080") // Port that server pass through
}

// PATH: server.go, entity folder -> video.go, service folder -> video service.go, controller folder -> video controller.go
