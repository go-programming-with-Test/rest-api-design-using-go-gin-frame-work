// this controller folder having a handler of GET all the list of videos and POST the new videos
package controller

import (
	"gilab.com/pragmaticreviews/golang-gin-poc/entitiy"
	"gilab.com/pragmaticreviews/golang-gin-poc/service"
	"github.com/gin-gonic/gin"
)

type VideoController interface {
	FindAll() []entitiy.Video
	Save(ctx gin.Context) entitiy.Video
}

type controller struct {
	service service.VideoService
}

func New(service service.VideoService) VideoController {
	var cs = controller{
		service: service,
	}
	return &cs
}

func (c *controller) FindAll() []entitiy.Video {
	return c.service.FindAll()
}
func (c *controller) Save(ctx gin.Context) entitiy.Video {
	var video entitiy.Video
	ctx.BindJSON(&video)
	c.service.Save(video)
	return video
}
